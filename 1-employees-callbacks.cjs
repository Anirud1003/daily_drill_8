let fs = require('fs')
let path = require('path')

// 1. Retrieve data for ids : [2, 13, 23].

function retriveData(idArray) {
    return fs.readFile(path.join(__dirname, "./data.json"), (error, data) => {
        if (error) {
            console.log(error)
        } else {
            let dataArray = JSON.parse(data).employees.filter((data) => {
                if (idArray.includes(data.id)) {
                    return data
                }
            })
            fs.writeFile(path.join(__dirname, 'output1.json'), JSON.stringify(dataArray,null,2), (error) => {
                if (error) {
                    console.log(error)
                } else {
                    console.log('files written')
                }
            })
        }
    })
}
retriveData([2, 13, 23])

// 2. Group data based on companies.
// { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

function groupData() {
    fs.readFile(path.join(__dirname, "./data.json"), (error, data) => {
        if (error) {
            console.log(error)
        } else {
            let companyGroup = JSON.parse(data).employees.reduce((accu, info) => {
                if (accu.hasOwnProperty(info.company)) {
                    accu[info.company].push(info)
                } else {
                    accu[info.company] = []
                    accu[info.company].push(info)
                }
                return accu
            }, {})
            
            fs.writeFile(path.join(__dirname, 'output2.json'), JSON.stringify(companyGroup,null,2), (error) => {
                if (error) {
                    console.log(error)
                } else {
                    console.log('files written')
                }
            })
        }
    })
}
groupData()


// 3. Get all data for company Powerpuff Brigade


function retriveCompanyData(companyName) {
    return fs.readFile(path.join(__dirname,"./data.json"), (error, data) => {
        if (error) {
            console.log(error)
        } else {
            let infoArray = JSON.parse(data).employees.filter((info) => {
                if (info.company == companyName) {
                    return info
                }
            })
            fs.writeFile(path.join(__dirname, 'output3.json'), JSON.stringify(infoArray,null,2), (error) => {
                if (error) {
                    console.log(error)
                } else {
                    console.log('files written')
                    
                }
            })
        }
    })
}
retriveCompanyData("Powerpuff Brigade")

// 4. Remove entry with id 2.

function removingID(id) {
    return fs.readFile(path.join(__dirname, "./data.json"), (error, data) => {
        if (error) {
            console.log(error)
        } else {
            let infoArray = JSON.parse(data).employees.filter((info) => {
                if (info.id !== id) {
                    return info
                }
            })
            fs.writeFile(path.join(__dirname, 'output4.json'), JSON.stringify(infoArray,null,2), (error) => {
                if (error) {
                    console.log(error)
                } else {
                    console.log('files written')                    
                }
            })
        }
    })
}
removingID(2)



